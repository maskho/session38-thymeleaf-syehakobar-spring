package com.example.cobalagi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CobalagiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CobalagiApplication.class, args);
	}

}
