package com.example.cobalagi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {
    @GetMapping("/dashboard")
    public String greeting(@RequestParam(name = "name",required = false,
    defaultValue = "Tamu") String name, Model model){
        model.addAttribute("name",name);
        return "dashboard";
    }
    @GetMapping("/about")
    public String about(@RequestParam(name = "name",required = false,
            defaultValue = "Tamu") String name, Model model){
        model.addAttribute("name",name);
        return "about";
    }
    @GetMapping("/product")
    public String product(@RequestParam(name = "name",required = false,
            defaultValue = "Tamu") String name, Model model){
        model.addAttribute("name",name);
        return "product";
    }

}
